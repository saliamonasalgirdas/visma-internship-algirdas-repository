import * as constant from '../Utils/constants.js';
import {RestfulService} from '../Utils/service.js';

export class SaveEditEventForm {
    constructor() {
        this.itemId = constant.WINDOW_URL.searchParams.get('id');
        //TODO if into init?
        if (this.itemId) {
            this.selectSaveButton();
            this.selectDeleteButton();

            this.deleteButton.classList.add('display-block');
            //TODO iskelt i metoda.
            RestfulService.get(`/events/${ this.itemId }`)
                .then(this.fillIEventForm.bind(this));

            this.button.addEventListener('click', () => this.editExistingEvent());
            this.deleteButton.addEventListener('click', () => this.deleteEvent());
            return;
        }

        this.selectSaveButton();
        this.button.addEventListener('click', () => this.saveEvent());
    }
    //TODO i init.
    selectSaveButton() {
        this.button = document.querySelector('.save-event-btn');
    }

    selectDeleteButton() {
        this.deleteButton = document.querySelector('.del-event-btn');
    }
    //TODO check deleteEvent..
    deleteEvent() {
        RestfulService.delete(`/events/${ this.itemId }`)
            .then(() => window.location = constant.INDEX);
    }

    editExistingEvent() {
        const eventName = document.getElementById('event-name').value;
        const eventDate = document.getElementById('date-event').value;
        const selectedEventType = document.getElementById('event-type').value;
        const eventObjectType = constant.eventType[selectedEventType];

        const event = {
            name: eventName,
            date: eventDate,
            type: eventObjectType
        };

        RestfulService.put(`/events/${ this.itemId }`, event)
            .then(() => window.location = constant.INDEX);
    }
//TODO to query selectors. doc.elementBYID.
    fillIEventForm(event) {
        document.getElementById('event-name').value = event.name;
        document.getElementById('date-event').value = event.date;
        document.getElementById('event-type').value = constant.eventType[event.type];
    }
//TODO events get value.
    saveEvent() {
        const eventName = document.getElementById('event-name').value;
        const eventDate = document.getElementById('date-event').value;
        const selectedEventType = document.getElementById('event-type').value;
        const eventObjectType = constant.eventType[selectedEventType];

        const event = {
            name: eventName,
            date: eventDate,
            type: eventObjectType
        };

        RestfulService.post('/events', event)
            .then(() => window.location = constant.INDEX);
    }
}