import * as constant from './constants.js';

export const getCalendarDay = (i) => `
<div class='weekday' id=${ i }>
    <div class='weekday-header'>
        <span></span><h2>${ i }</h2>
    </div>
    <div class='event-container'></div>
</div>`;

export const getWeekLabel = (i) => `
    <div class='week-label'>
        <span class='rotate-ninety'>${ i }</span>
    </div>`;

export const getEventTemplate = (event) => // event-widget onclick iskelti i JS
  `<div class="event-widget" onclick="window.location.href='..${ constant.COMPARE_ID }?id=${ event.id }'">
            <span class="truncate shadow ${ event.type === 1 ? 'daily-event' : 'daily-action' }">${ event.name }</span>
        </div>`;

export const getDayLabel = (days, day) =>
  `<span>${ day }</span>`;

export const getMonthLabel = (month, year) =>
  `<div class="month-navigation">
        <div class="show-month">
        <span>${ year }${ month }</span>
        </div>
        <div><span><i class="fas fa-arrow-left"></i></span></div>
        <div>
        <span><i class="fas fa-arrow-right"></i></span>
        </div>
    </div>`;

// export const getEventForm = () =>
//   `<form class="event-form">
//        <div class="separate">
//            <label>Your Event</label>
//            <input type="text" id="event-name" minlength="4" maxlength="32" required placeholder="Event name"/>
//        </div>
//        <div>
//            <input type="date" id="date-event" name="dateEvent"
//                   placeholder="2018-07-22"/>
//        </div>
//        <div>
//            <label>Is your event private?</label>
//            <div>
//                <select id="event-type">
//                    <option value="daily-event" class="select-options">Public</option>
//                    <option value="daily-action" class="select-options">Private</option>
//                </select>
//            </div>
//        </div>
//        <button type="button" class="save-event-btn">Save</button>
//        <button type="button" class="del-event-btn">Delete</button>
//    </form>`;
