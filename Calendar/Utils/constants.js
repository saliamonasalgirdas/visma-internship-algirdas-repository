export const backendUrl = 'http://localhost:3000';
export const eventType = {
  'daily-event': 1,
  'daily-action': 2,
  1: 'daily-event',
  2: 'daily-action'
};
export const monthName = {
  0: 'January',
  1: 'February',
  2: 'March',
  3: 'April',
  4: 'May',
  5: 'June',
  6: 'July',
  'January': 0,
  'February': 1,
  'March': 2,
  'April': 3,
  'May': 4,
  'June': 5,
  'July': 6
};
export const DAYS = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
export const WINDOW_URL = new URL(window.location.href);
export const INDEX = '/index.html';
export const COMPARE_ID = '/GetEditEventPage/create-events.html';
export const WEEK_DIVISION_NUMBER = 604800000;
export const DAYS_DIVISION_NUMBER = 86400000;
export const NUMBER_OF_DAYS_IN_A_WEEK = 7;