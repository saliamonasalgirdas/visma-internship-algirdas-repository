import * as template from '../Utils/template.js';
import {RestfulService} from '../Utils/service.js';
//TODO atgal i calendar
export class EventList {

  constructor(calendar){
    this.calendar = calendar;
    RestfulService.get('/events')
      .then(this.generateEvents.bind(this));
  } //TODO RestfulService.get i atskira metoda.

  insertEvent(event, index) {
    const eventContent = template.getEventTemplate(event);
    document.getElementsByClassName('event-container')[index - 1]
      .insertAdjacentHTML('beforeend', eventContent);
  }

  generateEvents(events) {
    events.forEach(item => {
      const dayNumber = new Date(item.date).getDate();
      const monthNumber = new Date(item.date).getMonth();

      if (monthNumber === this.calendar.month) {
        this.insertEvent(item, dayNumber);
      }
    });
  }
}


//TODO: structure. CALENDAR> insert GENERATE EVENT> DAY class> which inserts object into day
// const obj = {
//   1: [{}, {}, {}],
//     ...,
//     30: [{}, {}]
// }