import * as constant from '../Utils/constants.js';
import * as template from '../Utils/template.js';


export class Calendar {
  constructor() {
    this.today = new Date();
    this.month = new Date().getMonth();
    this.day = new Date().getDate();
    this.year = new Date().getFullYear();
    this.startDate = new Date('2018-01-01');
    this.date = new Date(this.today.getFullYear(), this.today.getMonth(), 1); // naming. arba i local variables
    this.endDate = new Date(this.today.getFullYear(), this.today.getMonth() + 1, 1);
    //TODO : init  generates.
    this.generateMonthLabel();
    this.generateDayNumbers();
    this.generateWeekLabels();
  }
//TODO padaryti unikalu metoda kuris priims skirtingus argumentus ir sustatys viska.
  getWeekOfTheMonth() {
    this.amountOfWeeks = ((this.date.getTime() - this.startDate.getTime())
        / constant.WEEK_DIVISION_NUMBER).toFixed(0);
    return Number(this.amountOfWeeks);
  }

  getDaysInTheMonth() {
    this.amountOfDays = ((this.endDate.getTime() - this.date.getTime())
        / constant.DAYS_DIVISION_NUMBER).toFixed(0);
    return Number(this.amountOfDays);
  }

  generateMonthLabel() {
    document.querySelector('.sidebar')
      .insertAdjacentHTML('beforeend', template.getMonthLabel(constant.monthName[this.month], this.year));
  }

  generateWeekLabels() {
    for (let weekNumber = 0; weekNumber < 5; weekNumber++) {
      document.querySelector('.week-labels')
        .insertAdjacentHTML('beforeend', template.getWeekLabel(this.getWeekOfTheMonth() + weekNumber));
    }
  }
//TODO consistency
  generateDayNumbers() {
    for (let dayNumber = 1; dayNumber <= this.getDaysInTheMonth(); dayNumber++) {
      document.querySelector('.calendar').insertAdjacentHTML('beforeend', template.getCalendarDay(dayNumber));

      if (this.day === dayNumber) {
        const highlightDay = document.querySelectorAll('.weekday')[dayNumber - 1];
        highlightDay.classList.add('highlight-day');
      }

      if (dayNumber <= constant.NUMBER_OF_DAYS_IN_A_WEEK) {
        document.querySelectorAll('.weekday-header')[dayNumber - 1]
          .insertAdjacentHTML('beforeend', template.getDayLabel(dayNumber + 1, constant.DAYS[dayNumber - 1]));
      }

    }
  }
}