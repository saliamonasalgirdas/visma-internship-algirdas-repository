import {Calendar} from './CalendarWindow/calendar.js';
import {SaveEditEventForm} from './GetEditEventPage/event-form.js';
import {EventList} from './CalendarWindow/event-list.js';

const app = (function () {
  const itemId = new URL(window.location.href).searchParams.get('id');
  const pathName = window.location.pathname;

  if (itemId || !(pathName === '/index.html' || pathName === '/')) { //TODO if end with return, dont write else.
      new SaveEditEventForm(); // instead of     const events = new EventList(calendar);
      return;
  }

  const calendar = new Calendar();
  new EventList(calendar);

});

document.addEventListener('DOMContentLoaded', () => {
  app();
});
//TODO component for CalendarWindow and geteditevnt.... COMPONENT FOLDER