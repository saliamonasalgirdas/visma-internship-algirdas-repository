module.exports = {
    "env": {
        "browser": true,
        "es6": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaVersion": 2016,
        "sourceType": "module"
    },
    "rules": {
        "no-return-assign": [
            "error",
            "except-parens"
        ],
        "no-magic-numbers": [
            "error",
            { "ignoreArrayIndexes": true }
        ],
        "no-extra-bind": [
            "error"
        ],
        "no-eq-null": [
            "error"
        ],
        "dot-location": [
            "error",
            "property"
        ],
        "curly": [
            "error"
        ],
        "template-curly-spacing": [
            "error",
            "always"
        ],
        "prefer-const": [
            "error"
        ],
        "prefer-arrow-callback": [
            "error"
        ],
        "no-var": [
            "error"
        ],
        "no-useless-return": [
            "error"
        ],
        "no-multiple-empty-lines": [
            "error"
        ],
        "no-nested-ternary": [
            "error"
        ],
        "yoda": [
            "error"
        ],
        "indent": [
            "error",
            2
        ],
        "linebreak-style": [
            "error",
            "windows"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],
        "no-irregular-whitespace": [
            "error"
        ],
        "object-curly-newline": [
            "error"
        ],
        "object-property-newline": [
            "error"
        ],
        "no-console": [
            "error", {
                "allow": [
                    "warn",
                    "error"
                ]
            }
        ],
    }
};